import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducers/politicalClojure';
import {LinearProgress} from 'material-ui';

class App extends Component {

    getChildContext() {
        return {
            actions: this.props.actions,
            PoliticalReducer: this.props.PoliticalReducer
        };
    }

    render() {
        const { progress } = this.props.PoliticalReducer;
        return (
            <div className="page-container">
                {this.props.children}
                <LinearProgress mode="determinate" color="#0288d1" value={progress} style={{position: 'fixed', bottom: 0, zIndex: 99999999, height: 7}}  />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

App.childContextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)