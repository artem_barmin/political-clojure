import React, { PropTypes, Component } from 'react';
import { DragDropContext } from 'react-dnd';
import { browserHistory } from 'react-router';
import classnames from 'classnames';
import  update from 'react-addons-update';
import Sortable from '../../components/global_components/Sortable';
import FaTrash from 'react-icons/lib/fa/trash';
import _ from 'lodash';

class DragAndDrop extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dropItems: [],
            rankedItems: [],
            alphas: _.range(
                'a'.charCodeAt(0),
                'z'.charCodeAt(0)+1
            )
        };
    }

    componentDidMount() {
        this.props.actions.progressBar(95);
    }

    componentWillMount() {
        this.props.actions.loadPROsOrCONs({id: this.props.chosenIssue.id, limit: this.props.limit, side: 1});
    }

    componentWillReceiveProps(props) {
        if (props.arguments !== this.props.arguments)
            this.setState({
                dropItems: props.arguments
            });
    }

    sortableContainersDecorator(componentBackingInstance) {
        // check if backing instance not null
        if (componentBackingInstance) {
            let options = {
                handle: ".group-title" // Restricts sort start click/touch to the specified element
            };
            Sortable.create(componentBackingInstance, options);
        }
    };

    initialListDecorator(componentBackingInstance) {
        // check if backing instance not null
        if (componentBackingInstance) {
            let options = {
                draggable: "div",
                group: {
                    name: 'initial'
                },
                ghostClass: 'lightblueBG',
                chosenClass: 'chosen',
                sort: false,
                animation: 100
            };
            Sortable.create(componentBackingInstance, options);
        }
    };

    sortableListDecorator(componentBackingInstance) {
        // check if backing instance not null
        if (componentBackingInstance) {
            let options = {
                draggable: "div", // Specifies which items inside the element should be sortable
                animation: 250,
                scrollSensitivity: 5,
                group: {
                    name: 'sorted',
                    put: ['initial', 'remove']
                },
                ghostClass: 'lightblueBG',
                chosenClass: 'chosen',
                onAdd: (evt) => {
                    var itemEl = evt.item;
                    let indexTemp = -1;
                    let addedElem = _.find(this.state.dropItems, (item) => {
                        return item.content === _.get(itemEl.textContent.match(/\)\s+(.*)/i), 1)
                    });
                    addedElem.rank = evt.newIndex+1;
                    if (this.state.rankedItems.length === 0) {
                        this.setState(update(this.state, {
                            rankedItems: {
                                $push: [addedElem]
                            }
                        }));
                    }
                    else {
                        indexTemp = _.indexOf(this.state.rankedItems, _.find(this.state.dropItems, (item) => {
                            return item.id === addedElem.id
                        }));
                        if ( indexTemp === -1 ) {
                            this.setState(update(this.state, {
                                rankedItems: {
                                    $push: [addedElem]
                                }
                            }));
                        }
                        else {
                            this.setState(update(this.state, {
                                rankedItems: {
                                    $splice: [[indexTemp, 1, addedElem]]
                                }
                            }));
                        }
                    }

                    this.props.actions.step6drag(this.state.rankedItems);
                },
                onSort: (evt) => {
                    let arrayOfNames = _.map(evt.target.childNodes, (node) => {
                        return _.get(node.textContent.match(/\)\s+(.*)/i), 1)
                    });

                    for (let i = 0; i < this.state.rankedItems.length; i++) {
                        for (let j = 0; j < arrayOfNames.length; j++) {
                            if(this.state.rankedItems[i].content === arrayOfNames[j])
                                this.state.rankedItems[i].rank = _.indexOf(arrayOfNames, arrayOfNames[j])+1;
                        }
                    }
                    this.props.actions.step6drag(this.state.rankedItems);
                }
            };
            Sortable.create(componentBackingInstance, options);
        }
    };

    removeListDecorator(componentBackingInstance) {
        // check if backing instance not null
        if (componentBackingInstance) {
            let options = {
                draggable: "div",
                group: {
                    name: 'remove',
                    put: ['initial', 'sorted']
                },
                sort: false,
                animation: 250,
                scrollSensitivity: 5,
                onAdd: (evt) => {
                    var itemEl = evt.item;
                    let indexTemp = -1;
                    let addedElem = _.find(this.state.dropItems, (item) => {
                        return item.content === _.get(itemEl.textContent.match(/\)\s+(.*)/i), 1)
                    });
                    addedElem.rank = 0;
                    if (this.state.rankedItems.length === 0) {
                        this.setState(update(this.state, {
                            rankedItems: {
                                $push: [addedElem]
                            }
                        }));
                    }
                    else {
                        indexTemp = _.indexOf(this.state.rankedItems, _.find(this.state.dropItems, (item) => {
                            return item.id === addedElem.id
                        }));
                        if ( indexTemp === -1 ) {
                            this.setState(update(this.state, {
                                rankedItems: {
                                    $push: [addedElem]
                                }
                            }));
                        }
                        else {
                            this.setState(update(this.state, {
                                rankedItems: {
                                    $splice: [[indexTemp, 1, addedElem]]
                                }
                            }));
                        }
                    }
                    this.props.actions.step6drag(this.state.rankedItems);
                },
                onSort: (evt) => {
                    let arrayOfNames = _.map(evt.target.childNodes, (node) => {
                        return _.get(node.textContent.match(/\)\s+(.*)/i), 1)
                    });

                    for (let i = 0; i < this.state.rankedItems.length; i++) {
                        for (let j = 0; j < arrayOfNames.length; j++) {
                            if(this.state.rankedItems[i].content === arrayOfNames[j])
                                this.state.rankedItems[i].rank = 0;
                        }
                    }
                    this.props.actions.step6drag(this.state.rankedItems);
                }
            };
            Sortable.create(componentBackingInstance, options);
        }
    };

    renderDropItems() {
        const { dropItems } = this.state;
        return _.map(dropItems, (card, index)=> {
            const letter = String.fromCharCode(index+97).toUpperCase();
            return (
                <div key={index} className="collection-item inline spans">{letter}) {card.content}</div>
            )
        })
    }

    finish() {
        browserHistory.push("/thank_you");
        this.props.actions.finish();
    }

    render() {
        let hidden = classnames({
            "hidden": false
        });
        return (
            <div className="main-card-container container z-depth-3 admin-container drd-container dnd"
                 ref={this.sortableContainersDecorator}>
                <div className="row fixed-parent">
                    <div className="col s9 m9 l6 fixed-args">
                        <span>Rank each of the arguments below relating to this proposal:</span>
                        <div className="collection" ref={this.initialListDecorator.bind(this)}>
                            {this.renderDropItems()}
                        </div>
                    </div>
                    <div className="col s3 m3 l6 right">
                        <div className="blue-grey darken-2 white-text drd-box">
                            <span className="hide-on-med-and-down">Drag good arguments here: (put stronger arguments higher up)</span>
                            <div className="row drd-container">
                                <div className="col s12 m12 l12 dropZone flex-container" ref='parUl'>
                                    <ul className="collection black-text rounded flex-child" ref={this.sortableListDecorator.bind(this)}>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="remove-zone flex-child">
                            <div className="white-text delete-box">
                                <span >Drag bad or irrelevant arguments here: <FaTrash className="margin-left-3 hide-on-med-and-down" /></span>
                                <div className="show-on-med-and-down hide-on-large-only custom-trash"><FaTrash /></div>
                                <ul className="collection no-border black-text rounded" ref={this.removeListDecorator.bind(this)}>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`row padding-right-3 padding-top-2 ${hidden} text-left finish-btn-container`}>
                    <button onClick={this.finish.bind(this)} className="btn light-blue darken-2 white-text">
                        Finish
                    </button>
                </div>
            </div>
        )
    }
}

DragAndDrop.propTypes = {
    arguments: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object.isRequired,
    limit: PropTypes.number
};


export default DragAndDrop;
