import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import DragAndDrop from './DragAndDrop';

class Step6 extends Component {
    render() {
        const { actions } = this.context;
        const { header, chosenIssue, sortedArgs, adminArgsToRank } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <DragAndDrop arguments={sortedArgs}
                             actions={actions}
                             chosenIssue={chosenIssue}
                             limit={adminArgsToRank}
                />
            </div>
        )
    }
}

Step6.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step6;
