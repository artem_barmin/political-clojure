import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MainContainer from './mainContainer';

import * as Actions from '../../reducers/politicalClojure';

class FinishSlide extends Component {

    componentDidMount() {
        this.props.actions.progressBar(100);
    }

    render() {
        return (
            <div className="main">
                <MainContainer/>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        PoliticalReducer: state.PoliticalReducer.toJS()
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(Actions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FinishSlide)
