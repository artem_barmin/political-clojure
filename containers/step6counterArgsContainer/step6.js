import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import DragAndDropCounerArgs from './DragAndDropCounerArgs';

class Step6counterArgs extends Component {
    render() {
        const { actions } = this.context;
        const { header, chosenIssue, sortedArgs, counterArgsAmountToRank, step5res } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <DragAndDropCounerArgs
                          arguments={sortedArgs}
                          actions={actions}
                          chosenIssue={chosenIssue}
                          step5res={step5res}
                          limit={counterArgsAmountToRank}
                />
            </div>
        )
    }
}

Step6counterArgs.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step6counterArgs;
