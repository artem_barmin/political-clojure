import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import CreateIssueContainer from './createIssueContainer';


class Step0 extends Component {

    render() {
        const { actions } = this.context;
        const { header, issues, adminArgsToRate } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <CreateIssueContainer issues={issues}
                                      actions={actions}
                                      limit={adminArgsToRate}
                />
            </div>
        );
    }
}

Step0.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step0;
