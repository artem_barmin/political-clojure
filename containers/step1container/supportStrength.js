import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import ChoosePosition from '../../components/step1components/choosePosition';
import { browserHistory, Link } from 'react-router';

class supportStrengthContainer extends Component {

    constructor (props) {
        super(props);
        this.state = {
            next: false
        };
    }

    componentDidMount() {
        this.props.actions.progressBar(16.9);
    }

    nextStep() {
        this.setState({
            next: true
        });
    }

    render() {
        let hide = classnames({
            "hidden": this.state.next === false
        });
        const { actions, chosenIssue } = this.props;
        return (
            <div className="main-card-container container z-depth-3">
                <span className="or-divider">Before thinking about it too much, would you say you support or oppose this proposition?</span>
                <ChoosePosition choosePos={actions.step1Strength}
                                issue={chosenIssue}
                                nextStep={this.nextStep.bind(this)}
                                />
                <div className={`row padding-right-3 padding-top-2 ${hide}`}>
                    <button onClick={() => browserHistory.push("/topic/"+chosenIssue.id+"/rate_argument")} className="btn light-blue darken-2 white-text right">
                        Next Step
                    </button>
                </div>
            </div>
        )
    }
}

supportStrengthContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object.isRequired
};

export default supportStrengthContainer;
