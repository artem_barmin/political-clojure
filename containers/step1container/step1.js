import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import SupportStrengthContainer from './supportStrength';

class Step1 extends Component {

    render() {
        const { actions } = this.context;
        const { header, chosenIssue } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <SupportStrengthContainer chosenIssue={chosenIssue}
                                          actions={actions}
                />
            </div>
        )
    }
}

Step1.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};


export default Step1;
