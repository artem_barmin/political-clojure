import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import RateContainer from './rateContainer';

class Step2 extends Component {

    render() {
        const { actions } = this.context;
        const { header, sortedArgs, chosenIssue, mistake, adminArgsToRate, initialPosition } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <RateContainer arguments={sortedArgs}
                               actions={actions}
                               chosenIssue={chosenIssue}
                               mistakesList={mistake}
                               initialPosition={initialPosition}
                               limit={adminArgsToRate}
                />
            </div>
        )
    }
}

Step2.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step2;
