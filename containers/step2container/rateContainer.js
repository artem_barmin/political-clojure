import React, { PropTypes, Component } from 'react';
import ReactDOM from 'react-dom';
import classnames from 'classnames';
import Slider from 'rc-slider';
import SingleArgRate from '../../components/step2components/singleArgRate';
import { browserHistory } from 'react-router';
import zenscroll from 'zenscroll';
import _ from 'lodash';

class ChooseArgStrength extends Component {

    constructor(props) {
        super(props);
        this.state = {
            itemToScroll: null
        };
    }

    componentDidMount() {
        this.props.actions.loadArgPairs({limit: 1000, topicId: this.props.chosenIssue.id});
        this.props.actions.progressBar(33.2);
    }

    componentWillMount() {
        if (this.props.initialPosition.strength < 2) {
            this.props.actions.loadPROsOrCONs({id: this.props.chosenIssue.id, limit: this.props.limit, side: -1});
        }
        else if(this.props.initialPosition.strength > 2) {
            this.props.actions.loadPROsOrCONs({id: this.props.chosenIssue.id, limit: this.props.limit, side: 1});
        }
        else {
            this.props.actions.loadPROsOrCONs({id: this.props.chosenIssue.id, limit: this.props.limit, side: _.sample([-1, 1])});
        }
    }

    componentWillReceiveProps(props) {
        if (props.arguments !== this.props.arguments)
            this.setState({
                arguments: _.forEach(props.arguments, (arg) => arg.support = 0)
            });
    }

    componentDidUpdate () {
        zenscroll.toY( ReactDOM.findDOMNode(this).scrollHeight, 1000);
    }

    renderArgument(argument, index) {
        let previous = index > 0 ? this.state.arguments[index - 1] : null;
        let bool;
        if (previous != null) {
            bool = previous.support != "0" && previous.suppStrength != null
        }
        let hidden = classnames({
            "hidden": bool === false
        });
        return (
            <SingleArgRate argument = {argument}
                           key = {index}
                           previous = {previous}
                           hideCSS = {hidden}
                           index = {index}
                           supportOrNot = {this.props.actions.step2Select}
                           choosePos = {this.props.actions.step2Strength}
                           pickMistake = {this.props.actions.pickMistake}
                           mistakesList = {this.props.mistakesList}
            />
        );
    }

    render() {

        let counter = 0;
        _.forEach(this.state.arguments, (argument) => {
            if (argument.support != "0" && argument.suppStrength != null) {
                counter++;
            }
        });
        return (
            <div className="main-card-container container z-depth-3">
                <span>Rate each of the arguments below relating to this proposal:</span>
                <div className="row no-margins" ref="argsList">
                    {_.map(this.state.arguments, this.renderArgument.bind(this))}
                </div>
                {(() => {
                    if ( counter === this.props.arguments.length) {
                        return(
                            <div className={`row padding-right-3 padding-top-2`}>
                                <button
                                    onClick={() => browserHistory.push("/topic/" + this.props.chosenIssue.id + "/find_counterarguments")}
                                    className="btn light-blue darken-2 white-text right">
                                    Next Step
                                </button>
                            </div>
                        )
                    }
                })()}

            </div>

        )
    }
}

ChooseArgStrength.propTypes = {
    actions: PropTypes.object.isRequired,
    arguments: PropTypes.array.isRequired,
    chosenIssue: PropTypes.object,
    initialPosition: PropTypes.object,
    limit: PropTypes.number,
    mistakesList: PropTypes.array.isRequired
};

export default ChooseArgStrength
