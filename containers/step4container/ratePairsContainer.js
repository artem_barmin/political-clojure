import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import { browserHistory } from 'react-router';
import  update from 'react-addons-update';
import _ from 'lodash';


class ratePairsContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inactive: true,
            strength: 0,
            pairs: [],
            nextStep: false,
            iterationCounter: 0,
            showConfirm: false
        };
    }

    componentWillMount() {
        this.props.actions.loadArgPairs({limit: this.props.iterationsLimit, topicId: this.props.chosenIssue.id});
    }

    componentWillReceiveProps(props) {
        if (props.pairs !== this.props.pairs)
            this.setState({
                pairs: props.pairs,
                selectedPair: _.sample(props.pairs)
            });
    }

    componentDidMount() {
        this.props.actions.progressBar(66.4);
    }

    confirm() {
        let reducedArgsArray = _.difference(this.state.pairs, this.state.selectedPair);
        let counter = this.state.iterationCounter;
        let pair = this.state.selectedPair;
        pair.strength = this.state.strength;
        this.setState({
            nextStep: true,
            inactive: true,
            iterationCounter: counter + 1,
            showConfirm: false
        });
        if (this.state.pairs.length <= 1 || this.state.iterationCounter === this.props.iterationsLimit - 1) {
            browserHistory.push("/topic/" + this.props.chosenIssue.id + "/decide")
        }
        else {
            setTimeout(()=> {
                this.setState({
                    nextStep: false
                });
                this.setState({
                    pairs: reducedArgsArray,
                    selectedPair: _.sample(reducedArgsArray)
                });
            }, 500);
        }
        this.props.actions.step4chooseSide(pair);
    }

    makeChoice(value) {
        this.setState({
            strength: value,
            inactive: false,
            showConfirm: true
        });
    }

    render() {
        let visibility = classnames({
            "hidden-box": this.state.nextStep
        });
        let inactive = classnames({
            "inactive": this.state.inactive === true
        });

        return (
            <div className="main-card-container container z-depth-3">
                <div className="row">
                    <div className="col s12 m6 l6">
                        <span>Argument (supporting):</span>
                        <div className={`argument visible-args ${visibility}`}>
                            {
                                (() => {
                                    if (this.state.selectedPair !== undefined)
                                        return (<span>{this.state.selectedPair.first.content}</span>)
                                })()
                            }
                        </div>
                    </div>
                    <div className="col s12 m6 l6 margin-top-3-xs">
                        <span>Argument (opposing):</span>
                        <div className={`argument visible-args ${visibility}`}>
                            {
                                (() => {
                                    if (this.state.selectedPair !== undefined)
                                        return (<span>{this.state.selectedPair.second.content}</span>)
                                })()
                            }
                        </div>
                    </div>
                </div>
                <div className="row des makeChoiceBox">
                    <span className="or-divider">Drag the slider to indicate <b>which side</b> of this argument pair is better: </span>
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Neutral</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Better</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Much better</span>
                        </div>
                    </div>
                    <div className="container">
                        <Slider className={`strength-slider ${inactive}`}
                                tipFormatter={null}
                                dots
                                min={0}
                                max={4}
                                onAfterChange={this.makeChoice.bind(this)}
                        />
                    </div>
                </div>
                {(() => {
                    if (this.state.showConfirm) {
                        return (
                            <div className={`col s12 padding-right-3 padding-top-2 right-align`}>
                                <button onClick={this.confirm.bind(this)}
                                        className="btn light-blue darken-2 white-text">
                                    Confirm
                                </button>
                            </div>
                        )
                    }
                })()}
            </div>
        );
    }
}

ratePairsContainer.propTypes = {
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object,
    pairs: PropTypes.array.isRequired,
    opposed: PropTypes.array.isRequired,
    iterationsLimit: PropTypes.number.isRequired
};

export default ratePairsContainer;