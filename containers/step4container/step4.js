import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import RatePairsContainer from '../step4container/ratePairsContainer';

class Step4 extends Component {

    render() {
        const { actions } = this.context;
        const { header, args, chosenIssue, argPairsAmountToRate, argPairsInfo } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <RatePairsContainer arguments={args}
                                    pairs={argPairsInfo}
                                    opposed={args}
                                    actions={actions}
                                    chosenIssue={chosenIssue}
                                    iterationsLimit={argPairsAmountToRate}
                />
            </div>
        )
    }
}

Step4.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step4;
