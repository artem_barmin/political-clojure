import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import CounterArgs from '../../components/step3components/counterArgs';

class Step3 extends Component {

    render() {
        const { actions } = this.context;
        const { header, args, sortedArgs, adminOneSide, chosenIssue, argumentsAmountToShow, argsAmountToPair, argPairsInfo} = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <CounterArgs arguments={args}
                             counterArgs={sortedArgs}
                             actions={actions}
                             chosenIssue={chosenIssue}
                             iterationLimit={argsAmountToPair}
                             oneSide={adminOneSide}
                             argPairs={argPairsInfo}
                             limit={argumentsAmountToShow}
                />
            </div>
        )
    }
}

Step3.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step3;
