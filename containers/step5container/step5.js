import React, { Component, PropTypes } from 'react';
import Header from '../../components/global_components/header';
import DecideContainer from './decideContainer';

class Step5 extends Component {

    render() {
        const { actions } = this.context;
        const { header, chosenIssue, rankCons, adminArgsToRank, step5res } = this.context.PoliticalReducer;
        return (
            <div className="main">
                <Header headName={header}/>
                <DecideContainer chosenIssue={chosenIssue}
                                 actions={actions}
                                 rankCons={rankCons}
                                 step5res={step5res}
                                 limit={adminArgsToRank}
                />
            </div>
        )
    }
}

Step5.contextTypes = {
    actions: React.PropTypes.object,
    PoliticalReducer: React.PropTypes.object
};

export default Step5;