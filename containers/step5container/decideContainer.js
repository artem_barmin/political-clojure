import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import ChoosePositionEnd from '../../components/step5components/choosePositionEnd';
import { browserHistory } from 'react-router';

class decideContainer extends Component {

    constructor (props) {
        super(props);
        this.state = {
            next: false,
            value: 0
        };
    }

    componentDidMount() {
        this.props.actions.progressBar(83);
    }

    nextStep() {
        if(!this.props.rankCons) {
            this.setState({
                next: true
            });
        }
    }

    goToPROsDrag() {
        browserHistory.push("/topic/"+this.props.chosenIssue.id+"/rank_argument");
    }

    goToCONsDrag() {
        browserHistory.push("/topic/"+this.props.chosenIssue.id+"/rank_counter_argument");
    }

    render() {
        let hidden = classnames({
            "hidden": !this.state.next
        });
        // let hidden = classnames({
        //     "hidden": !this.state.next || this.props.rankCons
        // });
        // let showCounterArgsRoute = classnames({
        //     "hidden": !this.props.rankCons
        // });
        let onClick = this.props.step5res.strength > 2 ? this.goToPROsDrag.bind(this) : this.goToCONsDrag.bind(this);
        return (
            <div className="main-card-container container z-depth-3">
                <span className="or-divider">After thinking about this in detail, would you say you support or oppose this proposition?</span>
                <ChoosePositionEnd  issue={this.props.chosenIssue}
                                    choosePosEnd={this.props.actions.step5Strength}
                                    nextStep={this.nextStep.bind(this)}
                />
                <div className={`row padding-right-3 padding-top-2 ${hidden}`}>
                    <button onClick={onClick} className="btn light-blue darken-2 white-text right">
                        Next Step
                    </button>
                </div>
            </div>
        )
    }
}

decideContainer.propTypes = {
    step5res: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object.isRequired,
    rankCons: PropTypes.bool.isRequired,
    limit: PropTypes.number
};

export default decideContainer
