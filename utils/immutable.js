import { Map, List, fromJS, Iterable } from 'immutable';
import { forIn, negate } from 'lodash';

function wrapConvertionToJs(fun)
{
    return (obj) =>
    {
        return fromJS(fun(obj.toJS ? obj.toJS() : obj));
    };
}

function wrapMutationToJs(fun)
{
    return (obj) =>
    {
        let jsObj = obj.toJS ? obj.toJS() : obj;
        fun(jsObj);
        return fromJS(jsObj);
    };
}

Map.prototype.mergeConcat = function (arg) {
    const mergeListsWithConcat = (prev, next, key) =>
    {
        if (next instanceof List && prev instanceof List)
        {
            return prev.concat(next);
        }
        else if (next instanceof Map && prev instanceof Map)
        {
            return next.mergeWith(mergeListsWithConcat, prev);
        }
        else if(prev instanceof Array && next instanceof List) {
// wtf? when adding new optional criteria after someone was changed, prev var appear as usual Array
            return fromJS(prev).concat(next);
        }
        else
        {
            return next;
        }
    }
    return this.mergeWith(mergeListsWithConcat, arg);
}

Map.prototype.removeBy = function (path, fun) {
    return this.updateIn(path, (arr) => arr.filter(negate(wrapConvertionToJs(fun))));
}

Map.prototype.updateBy = function (path, fun, updateFn) {
    console.log("IM FROM IMMUTABLE: ",path, fun, updateFn);
    const updateFnJs = wrapMutationToJs(updateFn);
    const funJs = wrapConvertionToJs(fun);
    return this.updateIn(path, (arr) => arr.map(ent => {
        if (funJs(ent))
        {
            return updateFnJs(ent);
        }
        else
        {
            return ent;
        }
    }));
}
