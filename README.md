### Usage

```
npm install
npm run start-dev ## Use this to start project in development mode
npm run buld ## Use to build project
```
## Database setup

```
Using postgresql console or PgAdmin3 create new connection;
Create database
Load 'schema.sql' from project's root folder. This will create tables for political database

In 'scripts/' find 'start-dev.sh'
Here you can edit database connection settings
```
Then open `http://localhost:3000` on your browser
Open 'http://localhost:3000/admin' to enter admin panel
Open 'http://localhost:3000/graphql' to enter PostGraphQL

## Deploying on Heroku:
Email: artem.barmin@gmail.com

Password: 1q2w3e4r

1. You'll need to install the Heroku Toolbelt to be able to deploy the app. (https://devcenter.heroku.com/articles/getting-started-with-nodejs#set-up)
2. Log in using the following command: $ heroku login
3. Add remote git url (if didn't add it yet): $ heroku git:remote -a political-clojure
4. $ git push heroku master

## Client + Server

Server written using automatic `GraphQL` schema generation