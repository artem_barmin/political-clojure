var path = require('path');
var webpack = require('webpack');
var InlineEnviromentVariablesPlugin = require('inline-environment-variables-webpack-plugin');
require = ('node-noop');

module.exports = {
    devtool: 'source-map',
    entry: [
        'babel-polyfill',
        'webpack-hot-middleware/client',
        './index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/static/'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),
        new webpack.optimize.DedupePlugin(),
        new InlineEnviromentVariablesPlugin(),
        new webpack.optimize.UglifyJsPlugin({minimize: true, compressor: {warnings: false}}),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        //new webpack.IgnorePlugin(/\/iconv-loader$/),
        new webpack.NormalModuleReplacementPlugin(/\/iconv-loader$/, 'node-noop')
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: ['babel'],
                exclude: /node_modules/,
                include: __dirname
            },
            {
                test: /\.css?$/,
                loaders: ['style', 'raw'],
                include: __dirname
            },
            {
                test: /\.json$/,
                loaders: ['json']
            }
        ]
    }
};
