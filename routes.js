import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';

import App from './containers/App';
import AuthContainer from './containers/userAuthContainer/authContainer';
import Step0 from './containers/step0container/step0';
import Step1 from './containers/step1container/step1';
import Step2 from './containers/step2container/step2';
import Step3 from './containers/step3container/step3';
import Step4 from './containers/step4container/step4';
import Step5 from './containers/step5container/step5';
import Step6 from './containers/step6container/step6';
import Step6counterArgs from './containers/step6counterArgsContainer/step6';
import finishSlide from './containers/finishSlideContainer/finishSlide';

export default (
    <Route path="/" component={App}>
        <IndexRoute name="auth" component={AuthContainer}/>
        <Route name="welcome" path="welcome" component={Step0}/>
        <Route name="topic" path="topic/:id/rate_topic" component={Step1}/>
        <Route name="topic" path="topic/:id/rate_argument" component={Step2}/>
        <Route name="topic" path="topic/:id/find_counterarguments" component={Step3}/>
        <Route name="topic" path="topic/:id/rate" component={Step4}/>
        <Route name="topic" path="topic/:id/decide" component={Step5}/>
        <Route name="topic" path="topic/:id/rank_argument" component={Step6}/>
        <Route name="topic" path="topic/:id/rank_counter_argument" component={Step6counterArgs}/>
        <Route name="finish" path="thank_you" component={finishSlide}/>
    </Route>
);