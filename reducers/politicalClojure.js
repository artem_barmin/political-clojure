import { createActionAsync, createReducer, createAction } from '../utils/actions';
import { fromJS } from 'immutable';
import axios from 'axios';
import _ from 'lodash';
import { Lokka } from 'lokka';
import { Transport } from 'lokka-transport-http';
import { map } from 'lodash';
import { persistent, client } from './persistent';

//Get data from server
export const loadIssues = createActionAsync("LOAD_ISSUES", loadIssuesData);
export const loadSingleIssue = createActionAsync("LOAD_SINGLE_ISSUE", loadPROsOrCONsData);
export const loadPROsOrCONs = createActionAsync("LOAD_PROS", loadPROsOrCONsData);
export const authorise = createActionAsync("SIGN_IN", findUser);
export const registerUser = createActionAsync("REGISTER", SignUp)
export const loadArgPairs = createActionAsync("LOAD_ARG_PAIRS", loadPairs);
export const getConfig = createActionAsync("GET_CONFIG", loadConfig);

//Fetch users answers
export const step1Strength = createAction("STEP1_SET_STRENGTH");
export const changeHeader = createAction("CHANGE_HEADER");
export const step2Strength = createAction("STEP2_SET_STRENGTH");
export const step2Select = createAction("STEP2_SELECT");
export const pickMistake = createAction("PICK_MISTAKE");
export const step3SelectCounterArg = createAction("STEP3_SELECT_COUNTER_ARGUMENT");
export const step4chooseSide = createAction("STEP4_CHOOSE_SIDE");
export const step5Strength = createAction("STEP5_SET_STRENGTH");
export const step6drag = createAction("STEP6_DRAG");
export const step6CONsDrag = createAction("STEP6_CONS_DRAG");
export const progressBar = createAction("PROGRESSBAR");
export const finish = createAction("FINISH");
//Load Config
function loadConfig() {
    return client.query(`query{
    configNodes{
      nodes {
        id
        key
        valueText
        valueNumber
        valueJson
      }
    }
  }`).then((res) => {
        const configs = res.configNodes.nodes;
        const getConfig = (key) => {
            const {valueText, valueNumber, valueJson} = _.find(configs, {
                key
            });
            return valueText || valueNumber || valueJson;
        };
        return {
            mistakes: getConfig("mistakes"),
            argsAmountToRate: getConfig("argsAmountToRate"),
            argsAmountToShow: getConfig("argsAmountToShow"),
            argsAmountToPair: getConfig("argsAmountToPair"),
            showFromOneSideOrFromBoth: getConfig("showFromOneSideOrFromBoth"),
            argPairsAmountToRate: getConfig("argPairsAmountToRate"),
            argAmountToRank: getConfig("argAmountToRank"),
            counterArgsAmountToRank: getConfig("counterArgsAmountToRank")
        }
    })
}
//Sign in
function findUser({login, pswd}) {
    return client.query(`query($login: String, $pswd: String){
      userNodes(login: $login, password: $pswd, deleted: false) {
        nodes {
          id
        }
      }
    }`, {
        login,
        pswd
    })
}

function SignUp({login, pswd}) {
    return client.query(`query($login: String, $pswd: String){
    userNodes(login: $login, password: $pswd, deleted: false) {
      nodes {
        id
      }
    }
  }`, {
        login,
        pswd
    }).then(({userNodes}) => {
        let usersArray = map(userNodes.nodes, 'id');
        if (_.isEmpty(usersArray)) {
            return client.mutate(`($login: String, $pswd: String){insertUser(input:{login: $login, password: $pswd})
          {
            user {
              id
              login
              password
            }
          }
      }
    `, {
                login,
                pswd
            });
        } else {
            return null;
        }
    });
}

//Load issues
function loadIssuesData() {
    return client.query(`query{
      topicNodes(offset:0, deleted: false){
        totalCount
        nodes {
          id
          name
        }
      }
    }`);
}

//Load PROs/CONs issues of chosen Issue----------------
function loadPROsOrCONsData(data) {
    return client.query(`query ($id:ID!, $limit: Int, $side: Int){
      topic(id: $id){
        id
        name
        argumentNodesByTopic(first: $limit, side: $side, orderBy: STRENGTH, descending: true, deleted: false) {
          edges {
            node {
              id
              content
              side
              userByAuthorId {
                id
                login
              }
            }
          }
        }
      }
    }`, {
        id: data.id,
        limit: data.limit,
        side: data.side
    })
}

function loadPairs({limit, topicId}) {
    return client.query(`query ($limit: Int, $topic: String){
  argumentPairingNodes (first: $limit, topicId: $topic, confirmed: true, deleted: false) {
    edges {
      node {
        id
        firstArg
        argumentByFirstArg
        {
          id content side
        }
        secondArg
        argumentBySecondArg {
          id content side
        }
      }
    }
  }
}`, {
        limit: limit,
        topic: topicId
    }).then(({argumentPairingNodes}) => {
        const argsPairs = _.map(argumentPairingNodes.edges, (edge) => {
            return edge.node
        });
        return argsPairs;
    });
}

export const PoliticalReducer = createReducer({

    //Load Config data-----------------------------------

    [getConfig.request]: (state) => {
        return state;
    },
    [getConfig.ok]: (state, {mistakes, argsAmountToRate, argsAmountToShow, argsAmountToPair, showFromOneSideOrFromBoth, argPairsAmountToRate, argAmountToRank, counterArgsAmountToRank}) => {
        return state.setIn(['mistake'], mistakes)
            .setIn(['adminArgsToRate'], argsAmountToRate)
            .setIn(['argumentsAmountToShow'], argsAmountToShow)
            .setIn(['argsAmountToPair'], argsAmountToPair)
            .setIn(['adminOneSide'], showFromOneSideOrFromBoth)
            .setIn(['argPairsAmountToRate'], argPairsAmountToRate)
            .setIn(['adminArgsToRank'], argAmountToRank)
            .setIn(['counterArgsAmountToRank'], counterArgsAmountToRank)
    },
    [getConfig.error]: (state, error) => {
        return state;
    },

    //Load users data-----------------------------------

    [authorise.request]: (state) => {
        return state;
    },
    [authorise.ok]: (state, {userNodes}) => {
        let usersArray = map(userNodes.nodes, 'id');
        if (_.isEmpty(usersArray)) {
            return state.setIn(['userFound'], false);
        } else {
            return state.setIn(['userFound'], true).setIn(['userId'], _.head(usersArray));
        }
    },
    [authorise.error]: (state, error) => {
        return state;
    },

    //Register new user-----------------------------------

    [registerUser.request]: (state) => {
        return state;
    },
    [registerUser.ok]: (state, data) => {
        if (data === null) {
            return state.setIn(['userExists'], true);
        } else {
            return state.setIn(['userExists'], false).setIn(['userId'], data.insertUser.user.id);
        }
    },
    [registerUser.error]: (state, error) => {
        return state;
    },

    //Load all data-----------------------------------

    [loadIssues.request]: (state) => {
        return state;
    },
    [loadIssues.ok]: (state, {topicNodes}) => {
        return state.setIn(['issues'], topicNodes.nodes);
    },
    [loadIssues.error]: (state, error) => {
        return state;
    },

    //Load all issues of chosen Issue------------------

    [loadSingleIssue.request]: (state, id) => {
        return state;
    },
    [loadSingleIssue.ok]: (state, {topic}) => {
        return state.setIn(['chosenIssue'], topic);
    },
    [loadSingleIssue.error]: (state, error) => {
        return state;
    },

    //Load PROs/CONs issues of chosen Issue-------------

    [loadPROsOrCONs.request]: (state, data) => {
        return state;
    },
    [loadPROsOrCONs.ok]: (state, {topic}) => {
        return state.setIn(['sortedArgs'], map(topic.argumentNodesByTopic.edges, 'node'));
    },
    [loadPROsOrCONs.error]: (state, error) => {
        return state;
    },

    //Load argument pairs-----------------------------------

    [loadArgPairs.request]: (state) => {
        return state;
    },
    [loadArgPairs.ok]: (state, data) => {
        const pairs = _.map(data, (pair) => {
            return {
                first: pair.argumentByFirstArg,
                second: pair.argumentBySecondArg,
                pairId: pair.id
            }
        });
        return state.setIn(['argPairsInfo'], pairs);
    },
    [loadArgPairs.error]: (state, error) => {
        return state;
    },

    //-------------------------------------------------

    [step1Strength]: (state, issueFetched, strength) => {
        let startChoice = {
            issueId: issueFetched.id,
            strength: strength
        };
        return state.setIn(['initialPosition'], startChoice);
    },
    [changeHeader]: (state, headerFetched) => {
        return state.setIn(['header'], headerFetched);
    },
    [progressBar]: (state, value) => {
        const bar = value;
        return state.setIn(['progress'], bar)
    },
    [step2Strength]: (state, argFetched, strength) => {
        const toJs = state.toJS();
        let newArgs = _.map(toJs.sortedArgs, (arg) => {
            if (arg.id === argFetched.id) {
                arg.suppStrength = strength;
                arg.mistake = -1;
            }
            return arg;
        });
        return state.setIn(['argumentRatings'], newArgs).setIn(['args'], newArgs);
    },
    [step2Select]: (state, argFetched, side) => {
        const toJs = state.toJS();
        let newArgs = _.map(toJs.sortedArgs, (arg) => {
            if (arg.id === argFetched.id) {
                arg.support = side;
                arg.otherMistake = ''
            }
            return arg;
        });
        return state.setIn(['argumentRatings'], newArgs).setIn(['args'], newArgs);
    },
    [pickMistake]: (state, argFetched, mistakeObj) => {
        const toJs = state.toJS();
        let newArgs = _.map(toJs.sortedArgs, (arg) => {
            if (arg.id === argFetched.id) {
                arg.mistake = mistakeObj.mistakeId;
                arg.otherMistake = mistakeObj.otherMistake;
            }
            return arg;
        });
        return state.setIn(['argumentRatings'], newArgs).setIn(['args'], newArgs);
    },
    [step3SelectCounterArg]: (state, fetchedArgPair) => {
        return state.updateIn(['argsPairs'], (oldArray) => oldArray.concat(fetchedArgPair));
    },
    [step4chooseSide]: (state, pairsObj) => {
        return state.updateIn(['step4pairs'], (oldArray) => oldArray.concat(pairsObj));
    },
    [step5Strength]: (state, issueFetched, strength) => {
        let endChoice = {
            issueId: issueFetched.id,
            strength: strength
        };
        if (strength === 2) {
            return state.setIn(['step5res'], endChoice).setIn(['rankCons'], true);
        }
        else return state.setIn(['step5res'], endChoice).setIn(['rankCons'], false);
    },
    [step6drag]: (state, fetchedArgs) => {
        return state.setIn(['step6resPros'], fetchedArgs)
    },

    //---------------------------------------------

    [step6CONsDrag]: (state, fetchedArgs) => {
        return state.setIn(['step6resCons'], fetchedArgs)
    },

    [finish]: persistent
}, fromJS(
    {
        progress: 0,
        userFound: null,
        userExists: null,
        usedId: '',
        //Admin initial state
        adminArgsToRate: 0,
        argumentsAmountToShow: 0,
        argsAmountToPair: 0,
        adminOneSide: "",
        argPairsAmountToRate: 0,
        adminArgsToRank: 0,
        counterArgsAmountToRank: 0,
        mistake: [],
        //End
        header: 'Welcome!',
        issues: [],
        sortedArgs: [], //Temp array for loading PROs or CONs issues
        argPairsInfo: [],
        chosenIssue: {}, //Step 0 result
        initialPosition: {}, //Step 1 result
        args: [],
        argumentRatings: [], //Step 2 results
        argsPairs: [], //Step 3 results
        step4pairs: [], //Step 4 result
        step5res: {}, //Step5 result
        rankCons: false, //Goto Step 6 ranks?
        step6resPros: [], //Step6 PROs result
        step6resCons: [], //Step6 CONs result
    }
));
