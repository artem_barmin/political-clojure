import { combineReducers } from 'redux';
import {PoliticalReducer} from './politicalClojure';

const rootReducer = combineReducers({
  PoliticalReducer
});

export default rootReducer
