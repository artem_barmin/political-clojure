#!/bin/bash

set -e

babel server --presets 'modern-node/4.0' --out-dir dist
babel ./public/admin/admin-es6.js --out-file ./public/admin/admin.js
webpack -p --config webpack.config.build.js
cp -v ./server/index.html ./dist
mv -v ./dist/bundle.js ./public/static/bundle.js
