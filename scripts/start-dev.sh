#!/bin/bash

export DEV=true
export DATABASE_URL="postgres://postgres:postgres@localhost:5432/political"
export PORT=3000
babel ./public/admin/admin-es6.js --out-file ./public/admin/admin.js
db-migrate up
babel-watch --presets 'modern-node/4.0' --watch server server/main.js
