'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var myApp = angular.module('myApp', ['ng-admin']);

var loadPerPage = 10;

var requestFields = {
  "topic": 'name',
  "argument": '\n  content\n  topic\n  topicByTopic\n  {\n    id\n    name\n  }\n  side\n  strength\n  ',
  "argumentPairing": '\n  id\n  topicId\n  firstArg\n  secondArg\n  argumentByFirstArg\n  {\n    id\n    content\n    topicByTopic\n    {\n      id\n      name\n    }\n  }\n  argumentBySecondArg\n  {\n    id\n    content\n    topicByTopic\n    {\n      id\n      argumentNodesByTopic\n      {\n        nodes\n        {\n          content\n        }\n      }\n      name\n    }\n  }\n  confirmed',
  "workSession": '\n  id\n  userByUserId {\n    id\n    login\n  }\n  topicByTopicId {\n    id\n    name\n  }\n  argumentRatingNodesByWorkSessionId{\n    nodes {\n      id\n    }\n  }\n  argumentPairingNodesByWorkSessionId{\n    nodes\n    {\n      id\n      firstArg\n    }\n  }\n  userViewNodesByWorkSessionId\n  {\n    nodes {\n      id\n      initial\n      view\n    }\n  }\n  ',
  "user": 'login password',
  "config": 'key description valueText valueNumber valueJson'
};

var jsonFields = {
  config: {
    newValueJson: true
  }
};

myApp.config(['NgAdminConfigurationProvider', function (nga) {
  var admin = nga.application('Political Admin').baseApiUrl(''); // main API endpoint

  var topic = nga.entity('topic'); // the API endpoint for users will be 'http://jsonplaceholder.typicode.com/users/:id
  var argument = nga.entity('argument');
  var argumentPairing = nga.entity('argumentPairing');
  var user = nga.entity('user');
  var config = nga.entity('config');
  var workSession = nga.entity('workSession');

  topic.listView().fields([nga.field('name').isDetailLink(true)]).perPage(loadPerPage);
  topic.creationView().fields([nga.field('name').validation({
    required: true,
    minlength: 3,
    maxlength: 100
  }), nga.field('arguments', 'referenced_list').targetEntity(argument).targetReferenceField('topic').targetFields([nga.field('content')]).listActions(['edit'])]);
  topic.editionView().fields(topic.creationView().fields());

  argument.listView().fields([nga.field('content').isDetailLink(true), nga.field('topicByTopic.name').label('Topic')]).perPage(loadPerPage);
  argument.creationView().fields([nga.field('content'), nga.field('topic', 'reference').label('Topic').targetEntity(topic).targetField(nga.field('name')).validation({
    required: true
  }), nga.field('side', 'choice').choices([{
    value: 1,
    label: 'PRO'
  }, {
    value: -1,
    label: 'CON'
  }]).validation({
    required: true
  }), nga.field('strength', 'choice').choices([{
    value: 1,
    label: "Fallacy"
  }, {
    value: 2,
    label: "Irrelevant"
  }, {
    value: 3,
    label: "Weak"
  }, {
    value: 4,
    label: "OK"
  }, {
    value: 5,
    label: "Strong"
  }]).validation({
    required: true
  })]);
  argument.editionView().fields(argument.creationView().fields());

  //-------------------------------------------------------------------------------
  argumentPairing.listView().fields([nga.field('argumentByFirstArg.content').isDetailLink(true).label('PRO'), nga.field('argumentByFirstArg.topicByTopic.name').label('Topic'), nga.field('argumentBySecondArg.content').label('CON'), nga.field('confirmed')]).perPage(loadPerPage);
  //On submit success
  argumentPairing.creationView().onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', '$window', function (progression, notification, $state, entry, entity, $window) {
    progression.done();
    notification.log('Argument Pair #' + entry._identifierValue + ' successfully created!!.', { addnCls: 'humane-flatty-success' });
    $state.go($state.get('create'), { entity: entity.name() }, { reload: true });
    // cancel the default action (redirect to the edition view)
    return false;
  }]);
  argumentPairing.creationView().fields([nga.field('topicId', 'reference').label('Topic').targetEntity(topic).targetField(nga.field('name')).validation({
    required: true
  }), nga.field('confirmed', 'boolean').choices([{ value: null, label: 'not yet decided' }, { value: true, label: 'true' }, { value: false, label: 'false' }]).validation({
    required: true
  }), nga.field('firstArg', 'reference').label('PRO').targetEntity(argument).targetField(nga.field('content')).template('<get-args arguments="datastore" entry="entry" side="1"></get-args>').remoteComplete(true).validation({
    required: true,
    validator: function validator(value) {
      // custom validation function
      if (value !== null) return value;else throw new Error('Pick First Arg!');
    }
  }), nga.field('secondArg', 'reference').label('CON').targetEntity(argument).targetField(nga.field('content')).template('<get-args arguments="datastore" entry="entry" side="-1"></get-args>').validation({
    required: true,
    validator: function validator(value) {
      // custom validation function
      if (value !== null) return value;else throw new Error('Pick Second Arg!');
    }
  })]);
  argumentPairing.editionView().onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', '$window', function (progression, notification, $state, entry, entity, $window) {
    progression.done();
    notification.log('Argument Pair #' + entry._identifierValue + ' successfully saved!!.', { addnCls: 'humane-flatty-success' });
    $state.go($state.get('list'), { entity: entity.name() }, { reload: true });
    // cancel the default action (redirect to the edition view)
    return false;
  }]);
  argumentPairing.editionView().fields([nga.field('topicId', 'reference').label('Topic').targetEntity(topic).targetField(nga.field('name')).editable(false).validation({
    required: true
  }), nga.field('confirmed', 'boolean').choices([{ value: null, label: 'not yet decided' }, { value: true, label: 'true' }, { value: false, label: 'false' }]).validation({
    required: true
  }), nga.field('firstArg', 'reference').label('PRO').targetEntity(argument).targetField(nga.field('content')).template('<get-args arguments="datastore" entry="entry" side="1"></get-args>').remoteComplete(true).validation({
    required: true,
    validator: function validator(value) {
      // custom validation function
      if (value !== null) return value;else throw new Error('Pick First Arg!');
    }
  }), nga.field('secondArg', 'reference').label('CON').targetEntity(argument).targetField(nga.field('content')).template('<get-args arguments="datastore" entry="entry" side="-1"></get-args>').validation({
    required: true,
    validator: function validator(value) {
      // custom validation function
      if (value !== null) return value;else throw new Error('Pick Second Arg!');
    }
  })]);

  user.listView().fields([nga.field('login').label("User's Login").isDetailLink(true)]).perPage(loadPerPage);
  user.creationView().fields([nga.field('login'), nga.field('password', 'password')]);
  user.editionView().fields(user.creationView().fields());

  config.listView().fields([nga.field('key').isDetailLink(true), nga.field('value').template("{{entry.values.valueText || entry.values.valueNumber || entry.values.valueJson}}")]);

  config.editionView().fields([nga.field('key').editable(false), nga.field('description', 'text').editable(false), nga.field('valueText'), nga.field('valueNumber', 'number'), nga.field('valueJson', 'embedded_list').targetFields([nga.field('name'), nga.field('description', 'text'), nga.field('examples', 'embedded_list').targetFields([nga.field('phrase', 'text')])])]).title('Edit config parameter "{{entry.values.key}}"');

  workSession.listView().fields([nga.field('userByUserId.login').label('User').isDetailLink(true), nga.field('topicByTopicId.name').label('Topic')]);
  workSession.showView().fields([nga.field('userByUserId.login').label('User'), nga.field('topicByTopicId.name').label('Topic'), nga.field('userViewNodesByWorkSessionId.nodes', 'embedded_list').label('User views').targetFields([nga.field('initial').defaultValue("false"), nga.field('view')])]);

  admin.addEntity(argument);
  admin.addEntity(argumentPairing);
  admin.addEntity(topic);
  admin.addEntity(user);
  admin.addEntity(config);
  admin.addEntity(workSession);

  admin.menu(nga.menu().addChild(nga.menu(topic).icon('<span class="glyphicon glyphicon-list"></span>')).addChild(nga.menu(argument).icon('<span class="glyphicon glyphicon-edit"></span>')).addChild(nga.menu(argumentPairing).icon('<span class="glyphicon glyphicon-link"></span>')).addChild(nga.menu(user).icon('<span class="glyphicon glyphicon-user"></span>')).addChild(nga.menu(config).icon('<span class="glyphicon glyphicon-edit"></span>')).addChild(nga.menu(workSession).icon('<span class="glyphicon glyphicon-edit"></span>')));

  nga.configure(admin);
}]);

function typeOfRequest(config) {
  console.log(config);
  var method = config.method;
  var url = config.url;

  if (method === "GET" && url.match(/^[a-zA-Z]*$/)) {
    return {
      type: "LIST",
      entity: url
    };
  } else if (method === "GET" && url.match(/.*\/.*/)) {
    var _url$match = url.match(/(.*)\/(.*)/);

    var _url$match2 = _slicedToArray(_url$match, 3);

    var _2 = _url$match2[0];
    var entity = _url$match2[1];
    var id = _url$match2[2];

    return {
      type: "GET",
      id: id,
      entity: entity
    };
  } else if (method === "PUT" || method === "DELETE") {
    var _url$match3 = url.match(/(.*)\/(.*)/);

    var _url$match4 = _slicedToArray(_url$match3, 3);

    var _3 = _url$match4[0];
    var _entity = _url$match4[1];
    var _id = _url$match4[2];

    return {
      type: method,
      id: _id,
      entity: _entity
    };
  } else if (method === "POST") {
    return {
      type: "POST",
      entity: url
    };
  }
}

function toMutationString(entity, fields) {
  return _.compact(_.map(fields, function (value, key) {
    if (value) {
      var serialized = JSON.stringify(value);
      if ((jsonFields[entity] || {})[key] && _.isObject(value)) {
        return key + ":" + '"' + serialized.replace(/"/g, "\\\"") + '"';
      } else if (!_.isObject(value)) {
        return key + ":" + serialized;
      }
    }
  })).join(",");
}
var graphQlHost = "/graphql/";

function RESTtoGraphQL(config) {
  var method = config.method;
  var params = config.params;
  var url = config.url;
  var data = config.data;

  var _typeOfRequest = typeOfRequest(config);

  var type = _typeOfRequest.type;
  var id = _typeOfRequest.id;
  var entity = _typeOfRequest.entity;

  console.log('config', type, entity, config);
  var fields = requestFields[entity];
  if (type === "LIST") {
    var _ref = params || {};

    var _filters = _ref._filters;

    var filterFields = toMutationString(entity, _filters);
    var sortDesc = true;
    var orderBy = '';
    console.log("CONFIG: ", params._sortField);
    if (params._sortDir === 'DESC') {
      sortDesc = true;
    } else sortDesc = false;
    if (entity === 'argumentPairing') {
      orderBy = 'TOPIC_ID';
    } else orderBy = 'ID';
    if (params._sortField === undefined && entity !== 'config') {
      return {
        method: "POST",
        headers: {},
        url: graphQlHost,
        data: {
          query: 'query{\n          ' + entity + 'Nodes(\n              offset: ' + (params._page - 1) * params._perPage + ',\n              ' + filterFields + ', orderBy: ' + orderBy + ' ,\n              descending: ' + sortDesc + ',\n              deleted: false\n            ){\n            nodes {\n              id\n              ' + fields + '\n            }\n            totalCount\n          }\n        }',
          variables: null
        }
      };
    } else if (entity !== 'config') {
      return {
        method: "POST",
        headers: {},
        url: graphQlHost,
        data: {
          query: 'query{\n          ' + entity + 'Nodes(\n              offset: ' + (params._page - 1) * params._perPage + ',\n              first: ' + params._perPage + ', ' + filterFields + ',\n              orderBy: ' + orderBy + ' ,descending: ' + sortDesc + ',\n              deleted: false\n            ){\n            nodes {\n              id\n              ' + fields + '\n            }\n            totalCount\n          }\n        }',
          variables: null
        }
      };
    }
    //config does not have 'deleted' value
    else {
        return {
          method: "POST",
          headers: {},
          url: graphQlHost,
          data: {
            query: 'query{\n          ' + entity + 'Nodes(\n              offset: ' + (params._page - 1) * params._perPage + ',\n              first: ' + params._perPage + ', ' + filterFields + ',\n              orderBy: ' + orderBy + ' ,descending: ' + sortDesc + ',\n            ){\n            nodes {\n              id\n              ' + fields + '\n            }\n            totalCount\n          }\n        }',
            variables: null
          }
        };
      }
  } else if (type === "GET") {
    return {
      method: "POST",
      headers: {},
      url: graphQlHost,
      data: {
        query: 'query{\n          ' + entity + '(id:"' + id + '"){\n            id\n            ' + fields + '\n          }\n        }',
        variables: null
      }
    };
  } else if (type === "PUT") {
    var modifiedFields = _l.mapKeys(_.omit(data, ['id']), function (v, key) {
      return 'new' + _l.upperFirst(key);
    });
    var mutationInput = toMutationString(entity, modifiedFields);
    return {
      method: "POST",
      headers: {},
      url: graphQlHost,
      data: {
        query: 'mutation{\n          update' + _l.upperFirst(entity) + '(input:{id:"' + id + '",' + mutationInput + '}){\n            ' + entity + '{\n              id\n            }\n          }\n        }',
        variables: null
      }
    };
  } else if (type === "POST") {
    var _mutationInput = toMutationString(entity, data);
    return {
      method: "POST",
      headers: {},
      url: graphQlHost,
      data: {
        query: 'mutation{\n          insert' + _l.upperFirst(entity) + '(input:{' + _mutationInput + '}){\n            ' + entity + '{\n              id\n            }\n          }\n        }',
        variables: null
      }
    };
  } else if (type === "DELETE") {
    return {
      method: "POST",
      headers: {},
      url: graphQlHost,
      data: {
        query: 'mutation{\n          update' + _l.upperFirst(entity) + '(input:{id:"' + id + '", newDeleted: true}){\n            ' + entity + '{\n              id\n            }\n          }\n        }',
        variables: null
      }
    };
  }
  return config;
}

myApp.config(function ($provide) {
  var provider = $provide;
  $provide.decorator('$http', ["$delegate", function ($delegate) {
    function $doggedHttp(config) {
      return $delegate(RESTtoGraphQL(config)).then(function (res) {
        var _typeOfRequest2 = typeOfRequest(config);

        var type = _typeOfRequest2.type;
        var entity = _typeOfRequest2.entity;

        console.log('result', res);
        if (res.data.errors) throw new Error(res.data.errors[0].message);
        switch (type) {
          case "LIST":
            var oldHeaders = res.headers;
            var totalCount = res.data.data[entity + "Nodes"].totalCount;
            return Object.assign(res, {
              data: res.data.data[entity + "Nodes"].nodes,
              headers: function headers(header) {
                if (header === "X-Total-Count") return totalCount;else return oldHeaders(header);
              }
            });
          case "GET":
            return Object.assign(res, {
              data: res.data.data[entity]
            });
          case "POST":
            return Object.assign(res, {
              data: res.data.data["insert" + _l.upperFirst(entity)][entity]
            });
          default:
            return res;
        }
      });
    }
    $doggedHttp.get = $delegate.get;
    return $doggedHttp;
  }]);
});

myApp.directive('getArgs', [function () {
  return {
    restrict: 'E',
    scope: {
      arguments: '&',
      entry: '&',
      side: '='
    },
    link: function link(scope) {
      scope.topic = scope.entry().values;
      scope.data = [];
      scope.pickedArg = null;

      scope.$watch('topic', function () {
        if (scope.topic.topicId !== null) {
          scope.data = scope.arguments()._entries.argument_1_choices.filter(function (argument) {
            if (argument.values.topic === scope.topic.topicId && argument.values.side === parseInt(scope.side)) return argument;
          });
        }
        scope.data.forEach(function (data) {
          if (data.values.id === scope.entry().values.firstArg || data.values.id === scope.entry().values.secondArg) {
            scope.pickedArg = data.values;
          }
        });
      }, true); //watcher end

      scope.pickArg = function (picked) {
        scope.pickedArg = picked.argument.values;
        if (scope.side === 1) {
          scope.entry().values.firstArg = scope.pickedArg.id;
        } else {
          scope.entry().values.secondArg = scope.pickedArg.id;
        }
      };
      return scope.pickedArg;
    },
    template: ' <ul class="args-list list-group">\n                    <li ng-click="pickArg(this)" ng-class="{active: argument.values === pickedArg}" class="list-group-item" ng-repeat="argument in data track by $index">\n                      <span>{{argument.values.content}}</span>\n                    </li>\n                </ul>\n               '
  };
}]);
