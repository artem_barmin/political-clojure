require("babel-core/register")
require("babel-polyfill")

import path from 'path'
import express from 'express'
import postgraphql from 'postgraphql'
import compress from 'compression'

var app = new express()
var port = process.env.PORT

if (process.env.DEV) {
  var webpackDevMiddleware = require('webpack-dev-middleware')
  var webpackHotMiddleware = require('webpack-hot-middleware')
  var webpack = require('webpack')
  var config = require('../webpack.config')
  var compiler = webpack(config)
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  }))
  app.use(webpackHotMiddleware(compiler))
}

app.use(compress())
app.use("/graphql/", postgraphql(process.env.DATABASE_URL))
app.use(express.static(path.join(__dirname, '../public')))

app.get("/*", function(req, res) {
  res.redirect("/");
})

app.listen(port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info("==> Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
  }
});
