var path = require('path')
var webpack = require('webpack')
require = ('node-noop');

module.exports = {
  devtool: 'cheap-module-eval-source-map',
  entry: [
    'babel-polyfill',
    'webpack-hot-middleware/client',
    './index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },

  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    //new webpack.IgnorePlugin(/\/iconv-loader$/),
    new webpack.NormalModuleReplacementPlugin(/\/iconv-loader$/, 'node-noop')
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loaders: ['babel'],
        exclude: /node_modules/,
        include: __dirname
      },
      {
        test: /\.css?$/,
        loaders: ['style', 'raw'],
        include: __dirname
      },
      {
        test: /\.json$/,
        loaders: ['json']
      }
    ],
    query: {
      presets: ['es2015']//, 'stage-0'
    }
  }
};
