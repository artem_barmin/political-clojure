"use strict";

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

var _express = require("express");

var _express2 = _interopRequireDefault(_express);

var _postgraphql = require("postgraphql");

var _postgraphql2 = _interopRequireDefault(_postgraphql);

var _compression = require("compression");

var _compression2 = _interopRequireDefault(_compression);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require("babel-core/register");
require("babel-polyfill");

var app = new _express2["default"]();
var port = process.env.PORT;

if (process.env.DEV) {
  var webpackDevMiddleware = require('webpack-dev-middleware');
  var webpackHotMiddleware = require('webpack-hot-middleware');
  var webpack = require('webpack');
  var config = require('../webpack.config');
  var compiler = webpack(config);
  app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
  }));
  app.use(webpackHotMiddleware(compiler));
}

app.use((0, _compression2["default"])());
app.use("/graphql/", (0, _postgraphql2["default"])(process.env.DATABASE_URL));
app.use(_express2["default"]["static"](_path2["default"].join(__dirname, '../public')));

app.get("/*", function (req, res) {
  res.redirect("/");
});

app.listen(port, function (error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NlcnZlci9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQU5BLFFBQVEscUJBQVI7QUFDQSxRQUFRLGdCQUFSOztBQU9BLElBQUksTUFBTSwwQkFBVjtBQUNBLElBQUksT0FBTyxRQUFRLEdBQVIsQ0FBWSxJQUF2Qjs7QUFFQSxJQUFJLFFBQVEsR0FBUixDQUFZLEdBQWhCLEVBQXFCO0FBQ25CLE1BQUksdUJBQXVCLFFBQVEsd0JBQVIsQ0FBM0I7QUFDQSxNQUFJLHVCQUF1QixRQUFRLHdCQUFSLENBQTNCO0FBQ0EsTUFBSSxVQUFVLFFBQVEsU0FBUixDQUFkO0FBQ0EsTUFBSSxTQUFTLFFBQVEsbUJBQVIsQ0FBYjtBQUNBLE1BQUksV0FBVyxRQUFRLE1BQVIsQ0FBZjtBQUNBLE1BQUksR0FBSixDQUFRLHFCQUFxQixRQUFyQixFQUErQjtBQUNyQyxZQUFRLElBRDZCO0FBRXJDLGdCQUFZLE9BQU8sTUFBUCxDQUFjO0FBRlcsR0FBL0IsQ0FBUjtBQUlBLE1BQUksR0FBSixDQUFRLHFCQUFxQixRQUFyQixDQUFSO0FBQ0Q7O0FBRUQsSUFBSSxHQUFKLENBQVEsK0JBQVI7QUFDQSxJQUFJLEdBQUosQ0FBUSxXQUFSLEVBQXFCLDhCQUFZLFFBQVEsR0FBUixDQUFZLFlBQXhCLENBQXJCO0FBQ0EsSUFBSSxHQUFKLENBQVEsK0JBQWUsa0JBQUssSUFBTCxDQUFVLFNBQVYsRUFBcUIsV0FBckIsQ0FBZixDQUFSOztBQUVBLElBQUksR0FBSixDQUFRLElBQVIsRUFBYyxVQUFTLEdBQVQsRUFBYyxHQUFkLEVBQW1CO0FBQy9CLE1BQUksUUFBSixDQUFhLEdBQWI7QUFDRCxDQUZEOztBQUlBLElBQUksTUFBSixDQUFXLElBQVgsRUFBaUIsVUFBUyxLQUFULEVBQWdCO0FBQy9CLE1BQUksS0FBSixFQUFXO0FBQ1QsWUFBUSxLQUFSLENBQWMsS0FBZDtBQUNELEdBRkQsTUFFTztBQUNMLFlBQVEsSUFBUixDQUFhLHlFQUFiLEVBQXdGLElBQXhGLEVBQThGLElBQTlGO0FBQ0Q7QUFDRixDQU5EIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJyZXF1aXJlKFwiYmFiZWwtY29yZS9yZWdpc3RlclwiKVxucmVxdWlyZShcImJhYmVsLXBvbHlmaWxsXCIpXG5cbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnXG5pbXBvcnQgZXhwcmVzcyBmcm9tICdleHByZXNzJ1xuaW1wb3J0IHBvc3RncmFwaHFsIGZyb20gJ3Bvc3RncmFwaHFsJ1xuaW1wb3J0IGNvbXByZXNzIGZyb20gJ2NvbXByZXNzaW9uJ1xuXG52YXIgYXBwID0gbmV3IGV4cHJlc3MoKVxudmFyIHBvcnQgPSBwcm9jZXNzLmVudi5QT1JUXG5cbmlmIChwcm9jZXNzLmVudi5ERVYpIHtcbiAgdmFyIHdlYnBhY2tEZXZNaWRkbGV3YXJlID0gcmVxdWlyZSgnd2VicGFjay1kZXYtbWlkZGxld2FyZScpXG4gIHZhciB3ZWJwYWNrSG90TWlkZGxld2FyZSA9IHJlcXVpcmUoJ3dlYnBhY2staG90LW1pZGRsZXdhcmUnKVxuICB2YXIgd2VicGFjayA9IHJlcXVpcmUoJ3dlYnBhY2snKVxuICB2YXIgY29uZmlnID0gcmVxdWlyZSgnLi4vd2VicGFjay5jb25maWcnKVxuICB2YXIgY29tcGlsZXIgPSB3ZWJwYWNrKGNvbmZpZylcbiAgYXBwLnVzZSh3ZWJwYWNrRGV2TWlkZGxld2FyZShjb21waWxlciwge1xuICAgIG5vSW5mbzogdHJ1ZSxcbiAgICBwdWJsaWNQYXRoOiBjb25maWcub3V0cHV0LnB1YmxpY1BhdGhcbiAgfSkpXG4gIGFwcC51c2Uod2VicGFja0hvdE1pZGRsZXdhcmUoY29tcGlsZXIpKVxufVxuXG5hcHAudXNlKGNvbXByZXNzKCkpXG5hcHAudXNlKFwiL2dyYXBocWwvXCIsIHBvc3RncmFwaHFsKHByb2Nlc3MuZW52LkRBVEFCQVNFX1VSTCkpXG5hcHAudXNlKGV4cHJlc3Muc3RhdGljKHBhdGguam9pbihfX2Rpcm5hbWUsICcuLi9wdWJsaWMnKSkpXG5cbmFwcC5nZXQoXCIvKlwiLCBmdW5jdGlvbihyZXEsIHJlcykge1xuICByZXMucmVkaXJlY3QoXCIvXCIpO1xufSlcblxuYXBwLmxpc3Rlbihwb3J0LCBmdW5jdGlvbihlcnJvcikge1xuICBpZiAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKVxuICB9IGVsc2Uge1xuICAgIGNvbnNvbGUuaW5mbyhcIj09PiBMaXN0ZW5pbmcgb24gcG9ydCAlcy4gT3BlbiB1cCBodHRwOi8vbG9jYWxob3N0OiVzLyBpbiB5b3VyIGJyb3dzZXIuXCIsIHBvcnQsIHBvcnQpXG4gIH1cbn0pO1xuIl19