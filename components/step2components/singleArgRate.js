import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import ReactDOM from 'react-dom';
import Slider from 'rc-slider';
import Popover from 'material-ui/lib/popover/popover';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import Tooltip from 'material-ui/lib';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import ReactTooltip from 'react-tooltip';
import ModalDialog from './modalDialog';
import _ from 'lodash';

const styles = {
    popover: {
        padding: 20
    },
    block: {
        maxWidth: 250
    },
    radioButton: {
        marginBottom: 16
    },
    customContentStyle: {
        width: '75%',
        maxWidth: 'none'
    }
};

class singleArgRate extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inactive: true,
            showBlock: false,
            modalOpen: false,
            sliderDisabled: true
        };
    }

    handleRequestClose (bool, mistake, otherMistake) {
        let mistakeObj = {
            mistakeId: mistake,
            otherMistake: otherMistake
        };
        this.setState({
            modalOpen: bool,
            mistakeObj: mistakeObj
        });
        this.props.pickMistake(this.props.argument, mistakeObj);
    }

    support(argument, e) {
        this.setState({
            sliderDisabled: false
        });
        this.props.supportOrNot(argument, e.target.value)
    }

    makeChoice(argument, value) {
        this.setState({
            inactive: false,
            anchorEl: this.refs.sliderComponent.refs.slider,
            showBlock: true,
            modalOpen: value === 0
        });
        this.props.choosePos(argument, value)
    }

    render() {
        let inactive = classnames({
            "inactive": this.state.inactive === true
        });
        let fadeout = classnames({
            "slider-fadeout": this.state.sliderDisabled
        });
        return (
            <div className={`row des makeChoiceBox ${this.props.hideCSS}`} ref="select">
                <div className="col s12 m6 l6 argument-container">
                    <div className="collection argument-box">
                        <div className="collection-item">{this.props.argument.content}</div>
                    </div>
                    <div className="input-field supp-op-box">
                        <div className="tooltip">
                            <span className="tooltiptext">Is it supporting or against?</span>
                            <select defaultValue="0" onChange={this.support.bind(this, this.props.argument)}>
                                <option value="0" disabled>Select:</option>
                                <option value="support">Supporting</option>
                                <option value="against">Against</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div className={`col s12 m6 l6 slider-container ${fadeout}`}>
                    <div className="row label-row">
                        <div className="col s12 radio-group">
                            <span>Fallacy</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Irrelevant</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Weak</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>OK</span>
                        </div>
                        <div className="col s12 radio-group">
                            <span>Strong</span>
                        </div>
                    </div>
                    <div className="container" ref="container">
                        <Slider onAfterChange={this.makeChoice.bind(this, this.props.argument)}
                                className={`strength-slider ${inactive}`}
                                tipFormatter={null}
                                disabled={this.state.sliderDisabled}
                                dots
                                min={0}
                                max={4}
                                ref="sliderComponent"
                        />
                        <Dialog title={`What kind of fallacy is it?`}
                                modal={true}
                                open={this.state.modalOpen}
                                className="modal-dialog-custom"
                                contentStyle={styles.customContentStyle}
                        >
                           <ModalDialog mistakes={this.props.mistakesList}
                                        closeDialog={this.handleRequestClose.bind(this)} />
                        </Dialog>
                    </div>
                </div>
            </div>
        )
    }
}

singleArgRate.propTypes = {
    argument: PropTypes.object.isRequired,
    supportOrNot: PropTypes.func.isRequired,
    choosePos: PropTypes.func.isRequired,
    pickMistake: PropTypes.func.isRequired,
    previous: PropTypes.object,
    hideCSS: PropTypes.string,
    mistakesList: PropTypes.array.isRequired
};

export default singleArgRate