import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import Slider from 'rc-slider';
import Popover from 'material-ui/lib/popover/popover';
import RaisedButton from 'material-ui/lib/raised-button';
import Dialog from 'material-ui/lib/dialog';
import RadioButton from 'material-ui/lib/radio-button';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import TextField from 'material-ui/lib/TextField';

import _ from 'lodash';

const styles = {
    popover: {
        padding: 20
    },
    block: {
        maxWidth: 250
    },
    radioButton: {
        marginBottom: 16
    },
    customContentStyle: {
        width: '75%',
        maxWidth: 'none'
    }
};

class modalDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            mistake: _.head(props.mistakes),
            mistakeId: 0,
            defaultSelected: '0',
            otherMistakeText: ''
        };
    }

    componentWillMount() {
        this.debounceInput = _.debounce((event) => {
            this.setState({
                otherMistakeText: event.target.value
            });
        }, 1000)
    }

    setOtherMistake(event) {
        event.persist();
        this.debounceInput(event);
    }

    handleRequestClose() {
        if (this.state.otherMistakeText !== '') {
            this.props.closeDialog(false, this.state.mistakeId, this.state.otherMistakeText);
        }
        else {
            this.props.closeDialog(false, this.state.mistakeId, '');
        }
    }

    changeMistake(event, index) {
        let mistakeValue = null;
        _.forEach(this.props.mistakes, (mistake ,index) => {
            index === parseInt(event.target.value) ? mistakeValue = mistake : null;
        });
        this.setState({
            mistake: mistakeValue,
            defaultSelected: event.target.value,
            mistakeId: parseInt(index)
        });
    }

    renderMistakesList(mistake, index) {
        return (
            <RadioButton
                key={index}
                value={`${index}`}
                label={mistake.name}
                style={styles.radioButton}
                onCheck={this.changeMistake.bind(this, mistake, index)}
            />
        )
    }

    renderExamples(example, index) {
        return (
            <div className="example row">
                <span className="example-header"><b>Example {index+1}</b>: "{example.phrase}"</span>
            </div>
        )
    }


    render() {
        return (
            <div className="row z-depth-1">
                <div className="row flex-container-row">
                    <div className="col s4 m4 l4 modalSideBar flex-child">
                        <RadioButtonGroup name="shipSpeed"
                                          className="white-text radioGroup"
                                          defaultSelected={this.state.defaultSelected}
                                          onChange={this.changeMistake.bind(this)}
                        >
                            {_.map(this.props.mistakes, this.renderMistakesList.bind(this))}
                        </RadioButtonGroup>
                    </div>
                    {
                        (() => {
                            if (this.state.mistakeId === this.props.mistakes.length - 1)
                                return (
                                    <div className="col s8 m8 l8 flex-child">

                                        <div className="description row">
                                            <b className="bold-text">Write your own option: </b>
                                            <TextField
                                                    className="expanded-input"
                                                    onChange={this.setOtherMistake.bind(this)}
                                                    floatingLabelText="Enter your option"
                                                    multiLine={true}
                                                    rows={2}
                                            />
                                        </div>
                                    </div>
                                )

                            else
                                return (
                                    <div className="col s8 m8 l8 flex-child">

                                        <div className="description row">
                                            <b className="bold-text">Definition: </b>{this.state.mistake.description}
                                        </div>
                                        {_.map(this.state.mistake.examples, this.renderExamples.bind(this))}
                                    </div>
                                )
                        })()
                    }

                </div>

                <div className="divider"></div>
                <div className="card-action cardFooter right-align">
                    <RaisedButton className="block custom-btn"
                                  label="Confirm!"
                                  onClick={this.handleRequestClose.bind(this)}
                    />
                </div>
            </div>

        )
    }
}

modalDialog.propTypes = {
    mistakes: PropTypes.array.isRequired,
    closeDialog: PropTypes.func.isRequired
};

export default modalDialog;