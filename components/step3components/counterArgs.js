import React, { PropTypes, Component } from 'react';
import classnames from 'classnames';
import SingleArgument from './argumentComponent';
import MdArrowForward from 'react-icons/lib/md/arrow-forward';
import MdArrowDownward from 'react-icons/lib/md/arrow-downward';
import { browserHistory } from 'react-router';
import  update from 'react-addons-update';
import NotificationSystem from 'react-notification-system';
import FlipMove from 'react-flip-move';
import ReactDOM from 'react-dom';
import zenscroll from 'zenscroll';
import _ from 'lodash';

class CounterArgs extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedArg: null,
            counterArgs: [],
            counterArgument: null,
            iterations: 0,
            randomArg: _.sample(props.counterArgs),
            nextArgs: false,
            nextStep: false
        }
    }

    componentDidMount() {
        this.props.actions.progressBar(49.8);
    }

    componentWillUpdate() {
        zenscroll.toY( ReactDOM.findDOMNode(this).scrollTop, 500);
    }

    componentWillMount() {
        this.props.actions.loadPROsOrCONs({id: this.props.chosenIssue.id});
    }

    componentWillReceiveProps(props) {
        if (props.counterArgs !== this.props.counterArgs) {
            const randPair = _.sample(props.argPairs);
            let counterArgs = props.counterArgs;
            if(props.limit < counterArgs.length) {
              counterArgs = _.reject(props.counterArgs, (arg) => {
                return arg && randPair.second && arg.id === randPair.second.id
              });
              let index = _.random(0, props.limit - 1);
              counterArgs[index] = randPair.second;
            }
            if(_.find(counterArgs, (argument) => {return argument}))
            this.setState({
                counterArgs: counterArgs,
                iterations: props.iterationLimit,
                currPair: randPair,
                randPairs: props.argPairs,
                side: randPair.second.side,
                randomArg: randPair.first
            });
        }
    }

    confirmChoice() {
        let pair = {
            argumentId: this.state.randomArg.id,
            counterArgumentId: this.state.counterArgument.id
        };
        let counter = this.state.iterations - 1;

        this.setState({
            nextStep: true,
            selectedArg: null,
            nextArgs: false
        });

        // Set timeout is needed for FADE-IN/FADE-OUT CSS transition
        setTimeout(()=> {
            // If iterator limit reduced to 1 or arguments count reduced to 1 we save last result and move to next step
            if (this.state.iterations === 1 || this.state.randPairs.length === 1) {
                browserHistory.push("/topic/" + this.props.chosenIssue.id + "/rate");
            }
            else {
                //Delete current random argument, because we found a pair for it
                const argPairs = _.reject(this.state.randPairs, (pair) => {
                    return pair.pairId === this.state.currPair.pairId;
                });

                let currPair = _.sample(argPairs);
                let reducedCounterArgsArray = this.state.counterArgs;
                if(this.props.limit < this.state.counterArgs.length) {
                    const index = _.random(0, this.props.limit - 1);
                    reducedCounterArgsArray = _.reject(this.state.counterArgs, (arg) => {
                        return arg.id === currPair.second.id
                    });
                    reducedCounterArgsArray = _.shuffle(reducedCounterArgsArray);
                    reducedCounterArgsArray[index] = currPair.second;
                }
                console.log("REDUCED: ", reducedCounterArgsArray);
                this.setState(update(this.state, {
                    counterArgs: {
                        $set: reducedCounterArgsArray
                    },
                    randomArg: {
                        $set: currPair.first
                    },
                    iterations: {
                        $set: counter
                    },
                    nextStep: {
                        $set: false
                    },
                    currPair: {
                        $set: currPair
                    },
                    randPairs: {
                        $set: argPairs
                    }
                }));
            }
        }, 500);
        this.props.actions.step3SelectCounterArg(pair);
    }

    selectArgument(counterArgument, obj) {
        let selectedIndex = _.indexOf(this.state.counterArgs, counterArgument);
        let firstElem = this.state.counterArgs[0];
        this.setState(update(this.state, {
                counterArgs: {
                    $splice: [
                        [0, 1, counterArgument],
                        [selectedIndex, 1, firstElem]
                    ]
                },
                counterArgument: {
                    $set: counterArgument
                },
                selectedArg: {
                    $set: obj
                },
                nextArgs: {
                    $set: true
                }
            }
        ));
    }

    renderCounterArguments() {
        let reducedArray = _.reject(this.state.counterArgs,(argument) => { return argument.id === this.state.randomArg.id; });
        if (this.props.oneSide === "one") {
            reducedArray = _.filter(reducedArray, (argument) => {
                if(argument.side === this.state.side)
                    return argument;
            });
        }
        reducedArray = reducedArray.slice(0, this.props.limit);
        return _.map(reducedArray, (argument, index) => {
            return (
                <SingleArgument key={argument.id}
                                argument={argument}
                                selectArgument={this.selectArgument.bind(this, argument)}
                                selected={this.state.selectedArg}
                />
            )
        });
    }

    render() {
        console.log("MOUNTED 2: ", this.state.counterArgs);
        let visibility = classnames({
            "hidden-box": this.state.nextStep
        });
        return (
            <div className="main-card-container container z-depth-3">
                <NotificationSystem ref="notificationSystem" />
                <div className="row">
                    <div className="col s12 m5 l5">
                        <span className="or-divider">For this argument:</span>
                        <div className={`arguments-box visible-args ${visibility}`}>
                            <div className="col s12">
                                <div className={`collection pointer`}>
                                    <div className={`collection-item inline light-blue darken-2 white-text`}>
                                        {this.state.randomArg.content}
                                    </div>
                                </div>
                                {(() => {
                                    if ( this.state.nextArgs ) {
                                        return(
                                            <div className={`row padding-top-2`} style={{paddingLeft: 13}}>
                                                <button
                                                    onClick={this.confirmChoice.bind(this)}
                                                    className="btn light-blue darken-2 white-text">
                                                    Confirm
                                                </button>
                                            </div>
                                        )
                                    }
                                })()}
                            </div>
                        </div>
                    </div>
                    <div className="col s12 m1 l1 flex-wrapper">
                        <MdArrowForward className="hide-600" />
                        <MdArrowDownward className="show-600" />
                    </div>
                    <div className="col s12 m5 l5">
                        <span className="or-divider">Select the best counterargument:</span>
                        <div className={`visible-args`}>
                            <FlipMove easing="ease" duration="500">
                                {this.renderCounterArguments()}
                            </FlipMove>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
CounterArgs.propTypes = {
    counterArgs: PropTypes.array.isRequired,
    argPairs: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired,
    chosenIssue: PropTypes.object,
    oneSide: PropTypes.string,
    iterationLimit: PropTypes.number,
    limit: PropTypes.number
};

export default CounterArgs
