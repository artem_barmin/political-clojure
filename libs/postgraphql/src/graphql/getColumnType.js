import { memoize } from 'lodash'
import { GraphQLNonNull, GraphQLID } from 'graphql'
import getType from './getType.js'

const getColumnType = memoize(({isNullable, type, isPrimaryKey}) => {
  if (isPrimaryKey)
    return GraphQLID;
  return (isNullable ? getType(type) : new GraphQLNonNull(getType(type)))
})

export default getColumnType
