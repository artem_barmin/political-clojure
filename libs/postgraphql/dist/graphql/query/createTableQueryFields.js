'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createSingleQueryField = require('./createSingleQueryField.js');

var _createSingleQueryField2 = _interopRequireDefault(_createSingleQueryField);

var _createNodesQueryField = require('./createNodesQueryField.js');

var _createNodesQueryField2 = _interopRequireDefault(_createNodesQueryField);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

/**
 * Creates the fields for a single table in the database. To see the type these
 * fields are used in and all the other fields exposed by a PostGraphQL query,
 * see `createQueryType`.
 *
 * @param {Table} table
 * @returns {GraphQLFieldConfig}
 */
const createTableQueryFields = table => {
  const fields = {};

  const singleField = (0, _createSingleQueryField2['default'])(table);
  const listField = (0, _createNodesQueryField2['default'])(table);

  // `createSingleQueryField` and others may return `null`, so we must check
  // for that.
  if (singleField) fields[table.getFieldName()] = singleField;
  if (listField) fields[`${ table.getFieldName() }Nodes`] = listField;

  return fields;
};

exports['default'] = createTableQueryFields;