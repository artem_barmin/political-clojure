'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _lodash = require('lodash');

var _symbols = require('../symbols.js');

var _createProcedureCall = require('./createProcedureCall.js');

var _createProcedureCall2 = _interopRequireDefault(_createProcedureCall);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; }

const resolveProcedure = (procedure, getProcedureArgs) => {
  // If this type is a table type, this variable will be a reference to that table.
  const returnTable = procedure.getReturnTable();
  const argEntries = Array.from(procedure.args);
  const procedureCall = (0, _createProcedureCall2['default'])(procedure);

  // Construct the query.
  //
  // If the procedure returns a table type let’s select all of its values
  // instead of just a tuple.
  const query = {
    name: `procedure_${ procedure.name }`,
    text: returnTable ? `select row_to_json(${ procedureCall }) as "output"` : `select ${ procedureCall } as "output"`
  };

  // Gets the output from a row returned by our query.
  const getOutput = _ref => {
    let output = _ref.output;

    if (!output) return null;

    // If we are returning a table, we need to make sure to add the row table
    // identifier property to our output object.
    if (returnTable) output[_symbols.$$rowTable] = returnTable;

    return output;
  };

  return (() => {
    var ref = _asyncToGenerator(function* (source, args, _ref2) {
      let client = _ref2.client;

      const procedureArgs = getProcedureArgs(source, args);

      // Actuall run the procedure using our arguments.
      const result = yield client.queryAsync({
        name: query.name,
        text: query.text,
        values: argEntries.map(function (_ref3) {
          var _ref4 = _slicedToArray(_ref3, 1);

          let name = _ref4[0];
          return procedureArgs[(0, _lodash.camelCase)(name)];
        })
      });

      // If the procedure returns a set, return all of the rows.
      if (procedure.returnsSet) return result.rows.map(getOutput);

      return getOutput(result.rows[0]);
    });

    return function (_x, _x2, _x3) {
      return ref.apply(this, arguments);
    };
  })();
};

exports['default'] = resolveProcedure;