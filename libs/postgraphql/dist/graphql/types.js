'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UUIDType = exports.JSONType = exports.IntervalType = exports.CircleType = exports.PointType = exports.DateType = exports.BigIntType = exports.PageInfoType = exports.CursorType = exports.NodeType = exports.fromID = exports.toID = undefined;

var _graphql = require('graphql');

/* ============================================================================
 * Utilities
 * ========================================================================= */

const toBase64 = value => new Buffer(value.toString()).toString('base64');
const fromBase64 = value => new Buffer(value.toString(), 'base64').toString();

const createStringScalarType = _ref => {
  let name = _ref.name;
  let description = _ref.description;
  return new _graphql.GraphQLScalarType({
    name,
    description,
    serialize: String,
    parseValue: String,
    parseLiteral: ast => ast.kind === _graphql.Kind.STRING ? ast.value : null
  });
};

/* ============================================================================
 * Node Types
 * ========================================================================= */

const toID = exports.toID = (tableName, values) => values;

const fromID = exports.fromID = id => {
  return {
    tableName: null,
    values: [id]
  };
};

const NodeType = exports.NodeType = new _graphql.GraphQLInterfaceType({
  name: 'Node',
  description: 'A single node object in the graph with a globally unique identifier.',
  fields: {
    id: {
      type: _graphql.GraphQLID,
      description: 'The `Node`’s globally unique identifier used to refetch the node.'
    }
  }
});

/* ============================================================================
 * Connection Types
 * ========================================================================= */

const CursorType = exports.CursorType = new _graphql.GraphQLScalarType({
  name: 'Cursor',
  description: 'An opaque base64 encoded string describing a location in a list of items.',
  serialize: toBase64,
  parseValue: fromBase64,
  parseLiteral: ast => ast.kind === _graphql.Kind.STRING ? fromBase64(ast.value) : null
});

const PageInfoType = exports.PageInfoType = new _graphql.GraphQLObjectType({
  name: 'PageInfo',
  description: 'Information about pagination in a connection.',
  fields: {
    hasNextPage: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean),
      description: 'Are there items after our result set to be queried?',
      resolve: _ref2 => {
        let hasNextPage = _ref2.hasNextPage;
        return hasNextPage;
      }
    },
    hasPreviousPage: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLBoolean),
      description: 'Are there items before our result set to be queried?',
      resolve: _ref3 => {
        let hasPreviousPage = _ref3.hasPreviousPage;
        return hasPreviousPage;
      }
    },
    startCursor: {
      type: CursorType,
      description: 'The cursor for the first item in the list.',
      resolve: _ref4 => {
        let startCursor = _ref4.startCursor;
        return startCursor;
      }
    },
    endCursor: {
      type: CursorType,
      description: 'The cursor for the last item in the list.',
      resolve: _ref5 => {
        let endCursor = _ref5.endCursor;
        return endCursor;
      }
    }
  }
});

/* ============================================================================
 * PostgreSQL Types
 * ========================================================================= */

const BigIntType = exports.BigIntType = createStringScalarType({
  name: 'BigInt',
  description: 'A signed eight-byte integer represented as a string'
});

const DateType = exports.DateType = createStringScalarType({
  name: 'Date',
  description: 'Some time value'
});

const PointType = exports.PointType = new _graphql.GraphQLObjectType({
  name: 'Point',
  description: 'A geometric point on a plane',
  fields: {
    x: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat),
      description: 'The x coordinate of the point'
    },
    y: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat),
      description: 'The y coordinate of the point'
    }
  }
});

const CircleType = exports.CircleType = new _graphql.GraphQLObjectType({
  name: 'Circle',
  description: 'Some circle on a plane made of a point and a radius',
  fields: {
    x: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat),
      description: 'The x coordinate of the circle'
    },
    y: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat),
      description: 'The y coordinate of the circle'
    },
    radius: {
      type: new _graphql.GraphQLNonNull(_graphql.GraphQLFloat),
      description: 'The radius of the circle'
    }
  }
});

const IntervalType = exports.IntervalType = new _graphql.GraphQLObjectType({
  name: 'Interval',
  description: 'Some time span',
  fields: {
    milliseconds: {
      type: _graphql.GraphQLInt
    },
    seconds: {
      type: _graphql.GraphQLInt
    },
    minutes: {
      type: _graphql.GraphQLInt
    },
    hours: {
      type: _graphql.GraphQLInt
    },
    days: {
      type: _graphql.GraphQLInt
    },
    months: {
      type: _graphql.GraphQLInt
    },
    years: {
      type: _graphql.GraphQLInt
    }
  }
});

const JSONType = exports.JSONType = new _graphql.GraphQLScalarType({
  name: 'JSON',
  description: 'An object not queryable by GraphQL(but supports serialization)',
  serialize: value => value,
  parseValue: value => value,
  parseLiteral: ast => ast.value
});

const UUIDType = exports.UUIDType = createStringScalarType({
  name: 'UUID',
  description: 'A universally unique identifier'
});